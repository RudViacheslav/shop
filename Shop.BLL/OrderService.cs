﻿using System;
using System.Collections.Generic;
using System.Linq;
using Shop.BLL.Entity;
using Shop.BLL.Infrastructure;
using Shop.BLL.Interfaces;
using Shop.DAL.Interfaces;
using DalE = Shop.DAL.Entity;

namespace Shop.BLL
{
    public class OrderService : EntitiyService, IOrderService
    {
        public OrderService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        /// <summary>
        ///     Gets  all orders of customer.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="addOrderItems">If set to <c>true</c>, ads to each order his OrderItems.</param>
        /// <returns></returns>
        public IEnumerable<Order> GetAllOrdersOfCustomer(int customerId, bool addOrderItems = false)
        {
            var dOrders = (from o in unitOfWork.Orders.All
                           where o.CustomerID == customerId
                           select o).ToList();

            var orders = Order.Map(dOrders);

            var customer = new Customer(unitOfWork.Customers.Read(customerId));

            if (addOrderItems)
            {
                for (int i = 0; i < orders.Count; i++)
                {
                    orders[i].Customer = customer;
                    int orderId = orders[i].Id;

                    var dOi = unitOfWork.OrderItems.All.Where(oi => oi.OrderId == orderId).ToList();

                    orders[i].OrderItems = OrderItem.Map(dOi);
                }
            }

            return orders;
        }

        public void BuyOrder(int orderId)
        {
            var order = unitOfWork.Orders.Read(orderId);

            if (order == null)
            {
                throw new ArgumentException($"Order with id {orderId} does not exist.");
            }

            // all orderitems in this order
            var orderItemsToBuy = unitOfWork.OrderItems.All
                                            .Where(oi => oi.OrderId == orderId).ToList();
            // finish order
            order.IsFinished = true;

            try
            {
                // decrease avalable products count
                for (int i = 0; i < orderItemsToBuy.Count; i++)
                {
                    orderItemsToBuy[i].Product.InStock -= orderItemsToBuy[i].Count;
                    unitOfWork.Products.Update(orderItemsToBuy[i].Product);
                }

                unitOfWork.Orders.Update(order);


                unitOfWork.SaveChanges();
            }
            catch (Exception e)
            {
                throw new OutOfStockException("Orderitem count more than available products count.");
            }
        }

        public Order GetUnfinishedOrder(int customerId)
        {
            var dOrder = (from o in unitOfWork.Orders.All
                          where o.CustomerID == customerId
                          where o.IsFinished == false
                          select o).FirstOrDefault();

            if (dOrder == null)
            {
                var customer = unitOfWork.Customers.Read(customerId);
                dOrder = unitOfWork.Orders.Create(new DalE.Order {CustomerID = customerId, Customer = customer, IsFinished = false});
                unitOfWork.SaveChanges();
            }

            return new Order(dOrder);
        }

        public Order GetById(int id)
        {
            var dOrder = (from o in unitOfWork.Orders.All
                          where o.Id == id
                          select o).FirstOrDefault();
            if (dOrder == null)
            {
                throw new ArgumentOutOfRangeException($"Id {id} does not exist.");
            }

            return new Order(dOrder);
        }
    }
}