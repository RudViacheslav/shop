﻿using Shop.DAL.Interfaces;

namespace Shop.BLL
{
    public abstract class EntitiyService
    {
        protected readonly IUnitOfWork unitOfWork;

        public EntitiyService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
    }
}