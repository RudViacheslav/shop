﻿using System;
using System.Collections.Generic;
using System.Linq;
using Shop.BLL.Entity;
using Shop.BLL.Interfaces;
using Shop.DAL.Interfaces;

namespace Shop.BLL
{
    public class CustomerService : EntitiyService, ICustomerService
    {
        public CustomerService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public Customer Create(string name, string password)
        {
            

            var exist = unitOfWork.Customers.All.Where(c => c.Name == name).FirstOrDefault();
            if (exist != null)
            {
                throw new ArgumentException("Customer with this username already exist.");
            }
            Validate(new Customer { Name = name, Password = password });
            var dCustomer = unitOfWork.Customers.Create(new DAL.Entity.Customer { Name = name, Password = password });
            unitOfWork.SaveChanges();
            return new Customer(dCustomer);
        }

        public Customer TryLogin(string name, string password)
        {
            var customer = GetByName(name);
            if (customer.Password != password)
                throw new ArgumentException("Wrong password");

            return customer;
        }
        public Customer GetById(int id)
        {
            //TODO add null check
            var dCustomer = unitOfWork.Customers.Read(id);
            if (dCustomer == null)
            {
                throw new ArgumentOutOfRangeException($"Customer with id {id} does not exist.");
            }

            return new Customer(dCustomer);
        }

        public Customer GetByName(string name)
        {
            var dCustomer = (from c in unitOfWork.Customers.All
                             where c.Name == name
                             select c).FirstOrDefault();
            if (dCustomer == null)
            {
                throw new ArgumentException("Wrong username");
            }

            return new Customer(dCustomer);
        }

        public IEnumerable<Customer> GetAll()
        {
            var dCustomers = unitOfWork.Customers.All.ToList();
            return Customer.Map(dCustomers);
        }

        private void Validate(Customer customer)
        {
            if (string.IsNullOrWhiteSpace(customer.Name) || customer.Name.Length < 3)
            {
                throw new ArgumentException("Name must be at leas 3 character length.");
            }

            if (string.IsNullOrWhiteSpace(customer.Password) || customer.Password.Length < 4)
            {
                throw new ArgumentException("Password must be at leas 4 character length.");
            }
        }
    }
}