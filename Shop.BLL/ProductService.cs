﻿using System;
using System.Collections.Generic;
using System.Linq;
using Shop.BLL.Entity;
using Shop.BLL.Infrastructure;
using Shop.BLL.Interfaces;
using Shop.DAL.Interfaces;

namespace Shop.BLL
{
    public class ProductService : EntitiyService, IProductService
    {
        public ProductService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public int GetCount()
        {
            return unitOfWork.Products.All.Count();
        }

        public Product Create(string name, int cost, int inStock)
        {
            Validate(new Product {Name = name, Cost = cost, InStock = inStock});

            var product = new Product(unitOfWork.Products.Create(
                                          new DAL.Entity.Product
                                          {
                                              Name = name,
                                              Cost = cost,
                                              InStock = inStock
                                          }));
            unitOfWork.SaveChanges();
            return product;
        }


        public void Update(Product product)
        {
            Validate(product);

            var dProduct = unitOfWork.Products.Read(product.Id);
            if (dProduct == null)
            {
                throw new ArgumentException("Product with such id is absent");
            }

            dProduct.Name = product.Name;
            dProduct.InStock = product.InStock;
            dProduct.Cost = product.Cost;

            unitOfWork.Products.Update(dProduct);

            unitOfWork.SaveChanges();
        }

        public Product GetById(int id)
        {
            var dProduct = unitOfWork.Products.All.Where(x => x.Id == id).FirstOrDefault();
            if (dProduct == null)
            {
                throw new ArgumentOutOfRangeException("Product with such id is absent");
            }

            return new Product(dProduct);
        }

        public IEnumerable<Product> GetAll()
        {
            var dProducts = unitOfWork.Products.All.ToList();
            var products = Product.Map(dProducts);

            return products;
        }

        public void Delete(int id)
        {
            unitOfWork.Products.Delete(id);
            unitOfWork.SaveChanges();
        }

        /// <summary>
        ///     Takes specified <paramref name="count" /> of ordered list of Products with ordering, skips first
        ///     <paramref name="skipCount" /> Products.
        /// </summary>
        /// <param name="count">The count.</param>
        /// <param name="skipCount">The skip.</param>
        /// <param name="orderBy">The order by.</param>
        /// <param name="ascending">if set to <c>true</c> [ascending].</param>
        /// <returns></returns>
        public IEnumerable<Product> GetAllOrdered(int count, int skipCount, ProductOrderby orderBy = ProductOrderby.Name, bool ascending = true)
        {
            var dProducts = unitOfWork.Products.All;

            IQueryable<DAL.Entity.Product> dOrderedProducts;

            if (ascending)
            {
                switch (orderBy)
                {
                    default:
                    case ProductOrderby.Name:
                        dOrderedProducts = dProducts.OrderBy(x => x.Name);
                        break;
                    case ProductOrderby.Cost:
                        dOrderedProducts = dProducts.OrderBy(x => x.Cost);
                        break;
                    case ProductOrderby.InStock:
                        dOrderedProducts = dProducts.OrderBy(x => x.InStock);
                        break;
                }
            }
            else
            {
                switch (orderBy)
                {
                    default:
                    case ProductOrderby.Name:
                        dOrderedProducts = dProducts.OrderByDescending(x => x.Name);
                        break;
                    case ProductOrderby.Cost:
                        dOrderedProducts = dProducts.OrderByDescending(x => x.Cost);
                        break;
                    case ProductOrderby.InStock:
                        dOrderedProducts = dProducts.OrderByDescending(x => x.InStock);
                        break;
                }
            }

            return Product.Map(dOrderedProducts.Skip(skipCount).Take(count).ToList());
        }

        private void Validate(Product product, bool checkCost = true, bool ckeckInStock = true, bool checkName = true)
        {
            if (product == null)
            {
                throw new ArgumentNullException();
            }

            if (product.Cost < 0 && checkCost)
            {
                throw new ArgumentException("Product's cost can't bee negative.");
            }

            if (product.InStock < 0 && ckeckInStock)
            {
                throw new ArgumentException("Poduct's in stock can't be negative");
            }

            if (string.IsNullOrWhiteSpace(product.Name) && checkName)
            {
                throw new ArgumentException("Product's name can't bee empty");
            }
        }
    }
}