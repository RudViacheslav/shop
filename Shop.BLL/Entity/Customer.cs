﻿using System.Collections.Generic;
using DalE = Shop.DAL.Entity;

namespace Shop.BLL.Entity
{
    public class Customer
    {
        public Customer()
        {
        }

        public Customer(DalE.Customer customer, bool addOrders = false)
        {
            Id = customer.Id;
            Name = customer.Name;
            Password = customer.Password;
            if (addOrders && customer.Orders != null)
                AddOrders(customer.Orders);
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }

        public IEnumerable<Order> Orders { get; set; }

        public static IEnumerable<Customer> Map(IEnumerable<DalE.Customer> customers)
        {
            var list = new List<Customer>();
            foreach (var item in customers)
            {
                list.Add(new Customer(item));
            }

            return list;
        }

        private void AddOrders(IEnumerable<DalE.Order> orders)
        {
            var ordersList = new List<Order>();
            foreach (var item in orders)
            {
                ordersList.Add(new Order(item));
            }

            Orders = ordersList;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}