﻿using System.Collections.Generic;
using System.Linq;
using DalE = Shop.DAL.Entity;

namespace Shop.BLL.Entity
{
    public class OrderItem
    {
        public OrderItem()
        {
        }

        public OrderItem(DalE.OrderItem orderItem)
        {
            Count = orderItem.Count;
            ProductId = orderItem.ProductId;
            OrderId = orderItem.OrderId;

            Product = new Product(orderItem.Product);
            Order = new Order(orderItem.Order);
        }

        public int Count { get; set; }
        public int ProductId { get; set; }
        public int OrderId { get; set; }

        public Product Product { get; set; }
        public Order Order { get; set; }

        public static IEnumerable<OrderItem> Map(IEnumerable<DalE.OrderItem> orderItems)
        {
            var list = new List<OrderItem>(orderItems.Count());
            foreach (var item in orderItems)
            {
                list.Add(new OrderItem(item));
            }

            return list;
        }
    }
}