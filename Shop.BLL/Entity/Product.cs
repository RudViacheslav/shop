﻿using System.Collections.Generic;
using DalE = Shop.DAL.Entity;

namespace Shop.BLL.Entity
{
    public class Product
    {
        public Product()
        {
        }

        public Product(DalE.Product product)
        {
            Id = product.Id;
            Name = product.Name;
            Cost = product.Cost;
            InStock = product.InStock;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int Cost { get; set; }

        /// <summary>
        ///     Count of products in stock.
        /// </summary>
        public int InStock { get; set; }

        public static IEnumerable<Product> Map(IEnumerable<DalE.Product> products)
        {
            var list = new List<Product>();

            foreach (var item in products)
            {
                list.Add(new Product(item));
            }

            return list;
        }
    }
}