﻿using System.Collections.Generic;
using DalE = Shop.DAL.Entity;

namespace Shop.BLL.Entity
{
    public class Order
    {
        public Order()
        {
        }

        public Order(DalE.Order order)
        {
            Id = order.Id;
            CustomerID = order.CustomerID;
            IsFinished = order.IsFinished;

            if (order.Customer != null)
            {
                Customer = new Customer(order.Customer);
            }
        }


        public int Id { get; set; }
        public int CustomerID { get; set; }
        public bool IsFinished { get; set; }

        public Customer Customer { get; set; }
        public IEnumerable<OrderItem> OrderItems { get; set; }

        public static List<Order> Map(IEnumerable<DalE.Order> orders)
        {
            var list = new List<Order>();

            foreach (var item in orders)
            {
                list.Add(new Order(item));
            }

            return list;
        }
    }
}