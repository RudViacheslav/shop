﻿using System;
using System.Collections.Generic;
using System.Linq;
using Shop.BLL.Entity;
using Shop.BLL.Infrastructure;
using Shop.BLL.Interfaces;
using Shop.DAL.Interfaces;

namespace Shop.BLL
{
    public class OrderItemService : EntitiyService, IOrderItemService
    {
        public OrderItemService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }


        public IEnumerable<OrderItem> GetAll()
        {
            return OrderItem.Map(unitOfWork.OrderItems.All.ToList());
        }

        public OrderItem GetByIds(int orderId, int productId)
        {
            return new OrderItem(unitOfWork.OrderItems.Read(orderId, productId));
        }

        public void Update(OrderItem orderItem)
        {
            Validate(orderItem);
            var dOrderItem = unitOfWork.OrderItems.Read(orderItem.OrderId, orderItem.ProductId);
            dOrderItem.Count = orderItem.Count;
            unitOfWork.OrderItems.Update(dOrderItem);

            unitOfWork.SaveChanges();
        }

        public void Delete(OrderItem orderItem)
        {
            var dOrderItem = unitOfWork.OrderItems.Read(orderItem.OrderId, orderItem.ProductId);
            unitOfWork.OrderItems.Delete(dOrderItem);
            unitOfWork.SaveChanges();
        }

        public IEnumerable<OrderItem> GetOrderItemsInOrder(int orderId)
        {
            var orderItems = from oi in unitOfWork.OrderItems.All
                             where oi.OrderId == orderId
                             select oi;

            return OrderItem.Map(orderItems);
        }

        public int GetOrderItemsCount(int orderId)
        {
            int count = (from oi in unitOfWork.OrderItems.All
                         where oi.OrderId == orderId
                         select oi).Count();

            return count;
        }

        public OrderItem GetOrCreateOrderitem(int productId, int orderId)
        {
            DAL.Entity.OrderItem orderItem = null;

            try
            {
                orderItem = (from oi in unitOfWork.OrderItems.All
                             where oi.OrderId == orderId
                             where oi.ProductId == productId
                             select oi).First();
            }
            catch (InvalidOperationException)
            {
                var order = unitOfWork.Orders.Read(orderId);
                var product = unitOfWork.Products.Read(productId);

                if (product.InStock < 1)
                {
                    throw new OutOfStockException("Produt is out of stock.");
                }

                orderItem = unitOfWork.OrderItems.Create(
                    new DAL.Entity.OrderItem
                    {
                        OrderId = orderId,
                        ProductId = productId,
                        Order = order,
                        Product = product,
                        Count = 1
                    });
                unitOfWork.SaveChanges();
            }


            return new OrderItem(orderItem);
        }

        private void Validate(OrderItem orderItem)
        {
            if (orderItem.Count < 0)
                throw new ArgumentOutOfRangeException("Count can't be negative.");
        }
    }
}