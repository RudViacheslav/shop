﻿using System.Collections.Generic;
using Shop.BLL.Entity;

namespace Shop.BLL.Interfaces
{
    public interface ICustomerService
    {
        Customer GetById(int id);
        Customer Create(string name, string password);
        Customer TryLogin(string name, string password);
        Customer GetByName(string name);
        IEnumerable<Customer> GetAll();
    }
}