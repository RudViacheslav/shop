﻿using System.Collections.Generic;
using Shop.BLL.Entity;
using Shop.BLL.Infrastructure;

namespace Shop.BLL.Interfaces
{
    public interface IProductService
    {
        int GetCount();
        Product Create(string name, int cost, int inStock);
        void Update(Product product);
        void Delete(int id);
        Product GetById(int id);
        IEnumerable<Product> GetAllOrdered(int count, int skip, ProductOrderby orderBy, bool ascending);
        IEnumerable<Product> GetAll();
    }
}