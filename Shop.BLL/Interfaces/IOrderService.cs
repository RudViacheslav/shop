﻿using System.Collections.Generic;
using Shop.BLL.Entity;

namespace Shop.BLL.Interfaces
{
    public interface IOrderService
    {
        /// <summary>
        ///     Gets unfinished order from Ccustomer orders.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns></returns>
        Order GetUnfinishedOrder(int customerId);

        void BuyOrder(int orderId);
        IEnumerable<Order> GetAllOrdersOfCustomer(int customerId, bool addOrderItems = false);
        Order GetById(int Id);
    }
}