﻿using System.Collections.Generic;
using Shop.BLL.Entity;

namespace Shop.BLL.Interfaces
{
    public interface IOrderItemService
    {
        IEnumerable<OrderItem> GetAll();
        OrderItem GetByIds(int orderId, int productId);
        void Update(OrderItem orderItem);
        void Delete(OrderItem orderItem);
        IEnumerable<OrderItem> GetOrderItemsInOrder(int orderId);
        OrderItem GetOrCreateOrderitem(int productId, int orderId);
        int GetOrderItemsCount(int orderId);
    }
}