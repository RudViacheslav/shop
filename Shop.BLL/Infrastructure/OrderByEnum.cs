﻿namespace Shop.BLL.Infrastructure
{
    public enum ProductOrderby
    {
        Name,
        Cost,
        InStock
    }
}