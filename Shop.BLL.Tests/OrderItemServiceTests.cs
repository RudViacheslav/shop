﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shop.BLL.Infrastructure;
using Shop.BLL.Interfaces;
using Shop.BLL.Tests.Mock;
using Shop.DAL.Entity;

namespace Shop.BLL.Tests
{
    [TestClass]
    public class OrderItemServiceTests
    {
        private IOrderItemService Initialize()
        {
            var dOrders = new List<Order>
            {
                new Order {CustomerID = 0, IsFinished = true, Id = 0},
                new Order {CustomerID = 0, IsFinished = true, Id = 1},
                new Order {CustomerID = 1, IsFinished = true, Id = 2},
                new Order {CustomerID = 1, IsFinished = false, Id = 3}
            };

            var dCustomers = new List<Customer>
            {
                new Customer {Id = 0, Name = "user1"},
                new Customer {Id = 1, Name = "user2"}
            };

            var dProducts = new List<Product>
            {
                new Product {Id = 0, Cost = 100, InStock = 2, Name = "aa"},
                new Product {Id = 1, Cost = 200, InStock = 1, Name = "bb"},
                new Product {Id = 2, Cost = 200, InStock = 0, Name = "cc"}
            };

            var dOrderItems = new List<OrderItem>
            {
                new OrderItem {OrderId = 0, ProductId = 0, Count = 1},
                new OrderItem {OrderId = 3, ProductId = 1, Count = 2} // wrong count
            };

            var uowMock = new UnitOfWorkMock(dCustomers, dProducts, dOrders, dOrderItems);
            var service = new OrderItemService(uowMock);

            return service;
        }


        [TestMethod]
        public void GetByIdsTest()
        {
            var service = Initialize();

            var expected = new Entity.OrderItem {OrderId = 0, ProductId = 0, Count = 1};
            var actual = service.GetByIds(0, 0);

            bool match = expected.OrderId == actual.OrderId &&
                         expected.ProductId == actual.OrderId &&
                         expected.Count == actual.Count;
            Assert.AreEqual(true, match);
        }

        [TestMethod]
        public void UpdateTest()
        {
            var service = Initialize();

            var orderItem = new Entity.OrderItem {OrderId = 0, ProductId = 0, Count = 5};
            ;

            service.Update(orderItem);

            var expected = orderItem;
            var actual = service.GetByIds(0, 0);

            bool match = expected.OrderId == actual.OrderId &&
                         expected.ProductId == actual.OrderId &&
                         expected.Count == actual.Count;
            Assert.AreEqual(true, match);
        }

        [TestMethod]
        public void DeleteTest()
        {
            var service = Initialize();
            var orderItem = service.GetByIds(0, 0);

            service.Delete(orderItem);
            var all = service.GetAll();

            Assert.AreEqual(1, all.Count());
        }

        [TestMethod]
        public void GetOrderItemsInOrderTest()
        {
            var service = Initialize();
            var items = service.GetOrderItemsInOrder(0);

            Assert.AreEqual(1, items.Count());
        }

        [TestMethod]
        public void GetOrderItemsCountTest()
        {
            var service = Initialize();
            int count = service.GetOrderItemsCount(0);

            Assert.AreEqual(1, count);
        }

        [TestMethod]
        public void GetOrCreateOrderitemTest_exist()
        {
            var service = Initialize();
            var orderItem = service.GetOrCreateOrderitem(0, 0);

            Assert.AreEqual(1, orderItem.Count);
        }

        [TestMethod]
        public void GetOrCreateOrderitemTest_new()
        {
            var service = Initialize();
            var orderItem = service.GetOrCreateOrderitem(1, 0);

            bool match = orderItem.Count == 1 &&
                         orderItem.ProductId == 1;

            Assert.AreEqual(true, match);
        }

        [TestMethod]
        public void GetOrCreateOrderitemTest_new_ZeroCount_throws_OutOfStock()
        {
            var service = Initialize();
            Assert.ThrowsException<OutOfStockException>(() => service.GetOrCreateOrderitem(2, 0));
        }
    }
}