﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Shop.BLL.Tests
{
    internal static class Shared
    {
        /// <summary>
        ///     Compares two collections by <paramref name="predicate" />.
        /// </summary>
        /// <typeparam name="T">Type of elemeents of collections.</typeparam>
        /// <param name="col1">First collection.</param>
        /// <param name="col2">Second collection.</param>
        /// <param name="predicate">Calls on each iteration, if it returns false once, loop function returns false.</param>
        /// <returns></returns>
        public static bool ParallelCompare<T>
        (
            IEnumerable<T> col1,
            IEnumerable<T> col2,
            Func<T, T, bool> predicate
        )
        {
            if (col1.Count() != col2.Count())
            {
                return false;
            }

            var col1Enumer = col1.GetEnumerator();
            var col2Enumer = col2.GetEnumerator();
            col1Enumer.Reset();
            col2Enumer.Reset();

            while (col1Enumer.MoveNext() &&
                   col2Enumer.MoveNext())
            {
                if (!predicate(col1Enumer.Current, col2Enumer.Current))
                {
                    return false;
                }
            }

            return true;
        }
    }
}