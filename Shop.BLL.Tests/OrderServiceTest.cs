﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shop.BLL.Infrastructure;
using Shop.BLL.Interfaces;
using Shop.BLL.Tests.Mock;
using BllE = Shop.BLL.Entity;
using DallE = Shop.DAL.Entity;

namespace Shop.BLL.Tests
{
    [TestClass]
    public class OrderServiceTest
    {
        private IOrderService Initialize()
        {
            var dOrders = new List<DallE.Order>
            {
                new DallE.Order {CustomerID = 0, IsFinished = true, Id = 0},
                new DallE.Order {CustomerID = 0, IsFinished = true, Id = 1},
                new DallE.Order {CustomerID = 1, IsFinished = true, Id = 2},
                new DallE.Order {CustomerID = 1, IsFinished = false, Id = 3}
            };

            var dCustomers = new List<DallE.Customer>
            {
                new DallE.Customer {Id = 0, Name = "user1"},
                new DallE.Customer {Id = 1, Name = "user2"}
            };

            var dProducts = new List<DallE.Product>
            {
                new DallE.Product {Id = 0, Cost = 100, InStock = 1, Name = "aa"},
                new DallE.Product {Id = 1, Cost = 200, InStock = 1, Name = "bb"}
            };

            var dOrderItems = new List<DallE.OrderItem>
            {
                new DallE.OrderItem {OrderId = 0, ProductId = 0, Count = 1},
                new DallE.OrderItem {OrderId = 3, ProductId = 1, Count = 2} // wrong count
            };

            var uowMock = new UnitOfWorkMock(dCustomers, dProducts, dOrders, dOrderItems);
            var orderService = new OrderService(uowMock);

            return orderService;
        }

        [TestMethod]
        public void GetAllOrdersOfCustomerTest()
        {
            var orderService = Initialize();

            var expected = new List<BllE.Order>
            {
                new BllE.Order {CustomerID = 0, IsFinished = true, Id = 0},
                new BllE.Order {CustomerID = 0, IsFinished = true, Id = 1}
            };

            var actual = orderService.GetAllOrdersOfCustomer(0);

            bool match = Shared.ParallelCompare(
                expected,
                actual,
                (e, a) => e.CustomerID == a.CustomerID &&
                          e.Id == a.Id &&
                          e.IsFinished == a.IsFinished);

            Assert.AreEqual(true, match);
        }

        [TestMethod]
        public void GetAllOrdersOfCustomerTest_includeOrderitems()
        {
            var orderService = Initialize();

            var expected = new List<BllE.Order>
            {
                new BllE.Order {CustomerID = 0, IsFinished = true, Id = 0},
                new BllE.Order {CustomerID = 0, IsFinished = true, Id = 1}
            };

            var actual = orderService.GetAllOrdersOfCustomer(0, true);

            bool match = Shared.ParallelCompare(
                expected,
                actual,
                (e, a) => e.CustomerID == a.CustomerID &&
                          e.Id == a.Id &&
                          e.IsFinished == a.IsFinished &&
                          a.OrderItems != null);

            Assert.AreEqual(true, match);
        }

        [TestMethod]
        public void BuyOrderTest()
        {
            var orderService = Initialize();

            orderService.BuyOrder(0);
            var order = orderService.GetById(0);
            bool match = order.IsFinished &&
                         order.Id == 0;

            Assert.AreEqual(true, match);
        }

        [TestMethod]
        public void BuyOrderTest_WrongOrder_throws()
        {
            var orderService = Initialize();

            Assert.ThrowsException<ArgumentException>(() => orderService.BuyOrder(70));
        }

        [TestMethod]
        public void BuyOrderTest_WrongCount_throws()
        {
            var orderService = Initialize();

            Assert.ThrowsException<OutOfStockException>(() => orderService.BuyOrder(3));
        }

        [TestMethod]
        public void GetUnfinishedOrderTest_ExistOrder()
        {
            var orderService = Initialize();
            var order = orderService.GetUnfinishedOrder(1);

            bool actual = order.CustomerID == 1 &&
                          !order.IsFinished &&
                          order.Id == 3;

            Assert.AreEqual(true, actual);
        }

        [TestMethod]
        public void GetUnfinishedOrderTest_NewOrder()
        {
            var orderService = Initialize();
            var order = orderService.GetUnfinishedOrder(0);

            bool actual = order.CustomerID == 0 &&
                          !order.IsFinished &&
                          order.Id == 4;
            Assert.AreEqual(true, actual);
        }

        [TestMethod]
        public void GetByIdTest()
        {
            var orderService = Initialize();
            var order = orderService.GetById(0);

            bool actual = order.CustomerID == 0
                          && order.Id == 0
                          && order.IsFinished;

            Assert.AreEqual(true, actual);
        }

        [TestMethod]
        public void GetByIdTest_WrongId_throws()
        {
            var orderService = Initialize();

            Assert.ThrowsException<ArgumentOutOfRangeException>(() => orderService.GetById(5));
        }
    }
}