﻿using Shop.BLL;
using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shop.BLL.Interfaces;
using Shop.BLL.Tests.Mock;
using BllE = Shop.BLL.Entity;
using DallE = Shop.DAL.Entity;

namespace Shop.BLL.Tests
{
    [TestClass]
    public class CustomerServiceTests
    {
        private ICustomerService Initialize(List<DallE.Customer> data)
        {
            var uowMock = new UnitOfWorkMock(data);
            var customerService = new CustomerService(uowMock);
            return customerService;
        }

        [TestMethod]
        public void CreateTest_ExistName_throw()
        {
            var customerService =
                Initialize(new List<DallE.Customer>
                {
                    new DallE.Customer {Id = 45, Name = "User2"}
                });


            Assert.ThrowsException<ArgumentException>(() => customerService.Create("User2", "1111"));
        }

        [TestMethod]
        public void GetByNameTest()
        {
            var customerService =
                Initialize(new List<DallE.Customer>
                {
                    new DallE.Customer {Id = 45, Name = "User2"}
                });


            var expected = customerService.Create("User3", "1111");
            var actual = customerService.GetByName("User3");

            Assert.AreEqual(expected.Name, actual.Name);
        }

        [TestMethod]
        public void GetByNameTest_WrongName()
        {
            var customerService =
                Initialize(new List<DallE.Customer>
                {
                    new DallE.Customer {Id = 45, Name = "User2"}
                });

            Assert.ThrowsException<ArgumentException>(() => customerService.GetByName("User0"));
        }

        [TestMethod]
        public void GetAllTest()
        {
            var customerService =
                Initialize(new List<DallE.Customer>
                {
                    new DallE.Customer {Id = 45, Name = "User2", Password="1111"},
                    new DallE.Customer {Id = 2, Name = "User3", Password="1111"}
                });

            var expected = new List<BllE.Customer>
            {
                new BllE.Customer {Id = 45, Name = "User2", Password="1111"},
                new BllE.Customer {Id = 2, Name = "User3", Password="1111"}
            };

            var actual = customerService.GetAll();
            bool match = Shared.ParallelCompare(actual, expected, (i, e) => i.Name == e.Name && i.Id == e.Id);

            Assert.AreEqual(true, match);
        }

        [TestMethod]
        public void GetByIdTest_Exist()
        {
            var customerService =
                Initialize(new List<DallE.Customer>
                {
                    new DallE.Customer {Id = 45, Name = "User2", Password="1111"}
                });

            var expected = customerService.Create("User3", "1111");
            var actual = customerService.GetById(0);

            Assert.AreEqual(expected.Id, actual.Id);
        }

        [TestMethod]
        public void GetByIdTest_NotExist_throws()
        {
            var customerService =
                Initialize(new List<DallE.Customer>
                {
                    new DallE.Customer {Id = 45, Name = "User2", Password="1111"}
                });

            Assert.ThrowsException<ArgumentOutOfRangeException>(() => customerService.GetById(5));
        }

        [TestMethod()]
        public void TryLoginTest()
        {
            var customerService =
                Initialize(new List<DallE.Customer>
                {
                    new DallE.Customer {Id = 45, Name = "User2", Password="1111"}
                });

            var expected = customerService.GetById(45);
            var actual = customerService.TryLogin("User2", "1111");

            bool match = expected.Id == actual.Id &&
                         expected.Name == actual.Name &&
                         expected.Password == actual.Password;

            Assert.AreEqual(true, match);
        }

        [TestMethod()]
        public void TryLoginTest_WrongPassword()
        {
            var customerService =
                Initialize(new List<DallE.Customer>
                {
                    new DallE.Customer {Id = 45, Name = "User2", Password="1111"}
                });

            Assert.ThrowsException<ArgumentException>(() => customerService.TryLogin("User2", "2222"));

        }

    }
}