﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shop.BLL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Shop.BLL.Entity.Tests
{
    [TestClass()]
    public class CustomerTests
    {
        [TestMethod()]
        public void MapTest()
        {
            List<DAL.Entity.Customer> dCustomers = new List<DAL.Entity.Customer>
            {
                new DAL.Entity.Customer{Id=0, Name= "aaa", Password="aaaa"},
                new DAL.Entity.Customer{Id=1, Name= "bbb", Password="bbbb"},
                new DAL.Entity.Customer{Id=2, Name= "ccc", Password="cccc"},
            };

            List<Customer> expected = new List<Customer>
            {
                new Customer{Id=0, Name= "aaa", Password="aaaa"},
                new Customer{Id=1, Name= "bbb", Password="bbbb"},
                new Customer{Id=2, Name= "ccc", Password="cccc"},
            };

            var actual = Customer.Map(dCustomers);

            bool match = Shop.BLL.Tests.Shared.ParallelCompare(
                expected, actual, (e, a) => e.Id == a.Id &&
                                            e.Name == a.Name&&
                                            e.Password == a.Password);
            Assert.AreEqual(true, match);
        }

        [TestMethod()]
        public void ToStringTest()
        {
            var customer = new Customer {Id = 0, Name = "aaa", Password = "aaaa"};
            string expected = "aaa";
            string actual = customer.ToString();

            Assert.AreEqual(expected, actual);
        }
    }
}