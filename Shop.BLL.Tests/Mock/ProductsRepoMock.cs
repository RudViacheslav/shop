﻿using System;
using System.Collections.Generic;
using System.Linq;
using Shop.DAL.Entity;
using Shop.DAL.Interfaces;

namespace Shop.BLL.Tests.Mock
{
    internal class ProductsRepoMock : Repository<Product>
    {
        private readonly List<Product> data;

        public ProductsRepoMock(List<Product> data) : base(null)
        {
            this.data = data;
        }

        public override IQueryable<Product> All
        {
            get
            {
                if (data != null)
                {
                    return data.AsQueryable();
                }

                return null;
            }
        }

        public override Product Create(Product item)
        {
            int id = GetUnusedId();
            data.Add(item);
            item.Id = id;
            return item;
        }

        public override Product Read(params object[] key)
        {
            int id = (int) key[0];
            return data.Where(p => p.Id == id).FirstOrDefault();
        }

        public override void Update(Product item)
        {
            if (item.InStock < 0)
            {
                throw new ArgumentOutOfRangeException("In stock count can't be negative.");
            }

            var product = data.Where(p => p.Id == item.Id).FirstOrDefault();
            if (product == null)
            {
                throw new ArgumentOutOfRangeException($"Product with id {item.Id} does not exist.");
            }

            product.Name = item.Name;
            product.InStock = item.InStock;
            product.Cost = item.Cost;
        }

        public override void Delete(Product item)
        {
            data.Remove(item);
        }

        public override void Delete(params object[] key)
        {
            var product = Read(key);
            data.Remove(product);
        }

        private int GetUnusedId()
        {
            for (int i = 0; i < int.MaxValue; i++)
            {
                bool exist = false;
                // check whether i exist in data as id
                foreach (var item in data)
                {
                    if (item.Id == i)
                    {
                        exist = true;
                        break;
                    }
                }

                // if i exist go to new loop
                if (exist)
                {
                    continue;
                }

                // else return i
                return i;
            }

            throw new InvalidOperationException();
        }
    }
}