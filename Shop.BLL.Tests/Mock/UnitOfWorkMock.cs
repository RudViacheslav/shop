﻿using System;
using System.Collections.Generic;
using System.Linq;
using Shop.DAL.Entity;
using Shop.DAL.Interfaces;

namespace Shop.BLL.Tests.Mock
{
    internal class UnitOfWorkMock : IUnitOfWork
    {
        public UnitOfWorkMock
        (
            List<Customer> customers = null,
            List<Product> products = null,
            List<Order> orders = null,
            List<OrderItem> orderItems = null
        )
        {
            Customers = new CustomerRepoMock(customers);
            Products = new ProductsRepoMock(products);
            Orders = new OrdersRepoMock(orders);
            OrderItems = new OrderItemsRepoMock(orderItems);
            Bind();
        }

        public Repository<Product> Products { get; }
        public Repository<Customer> Customers { get; }
        public Repository<Order> Orders { get; }
        public Repository<OrderItem> OrderItems { get; }

        public void SaveChanges()
        {
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        private void Bind()
        {
            if (OrderItems.All != null)
            {
                if (Orders.All != null)
                {
                    foreach (var item in Orders.All)
                    {
                        item.OrderItems = OrderItems.All.Where(oi => oi.OrderId == item.Id).ToList();
                    }
                }

                if (Orders.All != null)
                {
                    foreach (var item in OrderItems.All)
                    {
                        item.Order = Orders.All.Where(o => o.Id == item.OrderId).First();
                    }
                }

                if (Products.All != null)
                {
                    foreach (var item in OrderItems.All)
                    {
                        item.Product = Products.All.Where(p => p.Id == item.ProductId).First();
                    }
                }
            }
        }
    }
}