﻿using System;
using System.Collections.Generic;
using System.Linq;
using Shop.DAL.Entity;
using Shop.DAL.Interfaces;

namespace Shop.BLL.Tests.Mock
{
    internal class CustomerRepoMock : Repository<Customer>
    {
        private readonly List<Customer> data;

        public CustomerRepoMock(List<Customer> data) : base(null)
        {
            this.data = data;
        }

        public override IQueryable<Customer> All
        {
            get
            {
                if (data != null)
                {
                    return data.AsQueryable();
                }

                return null;
            }
        }

        public override Customer Create(Customer item)
        {
            int id = GetUnusedId();
            data.Add(item);
            item.Id = id;
            return item;
        }

        public override Customer Read(params object[] key)
        {
            int id = (int) key[0];
            return data.Where(c => c.Id == id).FirstOrDefault();
        }

        public override void Update(Customer item)
        {
            var customer = data.Where(c => c.Id == item.Id).FirstOrDefault();
            customer.Name = item.Name;
            customer.Orders = item.Orders;
        }

        public override void Delete(Customer item)
        {
            data.Remove(item);
        }

        public override void Delete(params object[] key)
        {
            var customer = Read(key);
            data.Remove(customer);
        }

        private int GetUnusedId()
        {
            for (int i = 0; i < int.MaxValue; i++)
            {
                bool exist = false;
                // check whether i exist in data as id
                foreach (var item in data)
                {
                    if (item.Id == i)
                    {
                        exist = true;
                        break;
                    }
                }

                // if i exist go to new loop
                if (exist)
                {
                    continue;
                }

                // else return i
                return i;
            }

            throw new InvalidOperationException();
        }
    }
}