﻿using System.Collections.Generic;
using System.Linq;
using Shop.DAL.Entity;
using Shop.DAL.Interfaces;

namespace Shop.BLL.Tests.Mock
{
    internal class OrderItemsRepoMock : Repository<OrderItem>
    {
        private readonly List<OrderItem> data;

        public OrderItemsRepoMock(List<OrderItem> orderItems) : base(null)
        {
            data = orderItems;
        }


        public override IQueryable<OrderItem> All
        {
            get
            {
                if (data != null)
                {
                    return data.AsQueryable();
                }

                return null;
            }
        }

        public override OrderItem Create(OrderItem item)
        {
            var exist = Read(item.OrderId, item.ProductId);
            if (exist == null)
            {
                data.Add(item);
                return item;
            }

            return exist;
        }

        public override OrderItem Read(params object[] key)
        {
            int orderId = (int) key[0];
            int productId = (int) key[1];

            var orderitem = data.Where(x =>
                                           x.OrderId == orderId &&
                                           x.ProductId == productId).FirstOrDefault();

            return orderitem;
        }

        public override void Update(OrderItem item)
        {
            var orderItem = Read(item.OrderId, item.ProductId);
            orderItem.Count = item.Count;
        }

        public override void Delete(OrderItem item)
        {
            var orderItem = Read(item.OrderId, item.ProductId);
            data.Remove(orderItem);
        }

        public override void Delete(params object[] key)
        {
            var orderItem = Read(key);
            data.Remove(orderItem);
        }
    }
}