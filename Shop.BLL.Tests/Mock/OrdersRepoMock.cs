﻿using System;
using System.Collections.Generic;
using System.Linq;
using Shop.DAL.Entity;
using Shop.DAL.Interfaces;

namespace Shop.BLL.Tests.Mock
{
    internal class OrdersRepoMock : Repository<Order>
    {
        private readonly List<Order> data;

        public OrdersRepoMock(List<Order> data) : base(null)
        {
            this.data = data;
        }

        public override IQueryable<Order> All
        {
            get
            {
                if (data != null)
                {
                    return data.AsQueryable();
                }

                return null;
            }
        }

        public override Order Create(Order item)
        {
            int id = GetUnusedId();
            data.Add(item);
            item.Id = id;
            return item;
        }

        public override Order Read(params object[] key)
        {
            int id = (int) key[0];
            return data.Where(c => c.Id == id).FirstOrDefault();
        }

        public override void Update(Order item)
        {
            var order = data.Where(c => c.Id == item.Id).FirstOrDefault();
            order.CustomerID = item.CustomerID;
            order.IsFinished = item.IsFinished;
            order.OrderItems = item.OrderItems;
        }

        public override void Delete(Order item)
        {
            data.Remove(item);
        }

        public override void Delete(params object[] key)
        {
            var order = Read(key);
            data.Remove(order);
        }

        private int GetUnusedId()
        {
            for (int i = 0; i < int.MaxValue; i++)
            {
                bool exist = false;
                // check whether i exist in data as id
                foreach (var item in data)
                {
                    if (item.Id == i)
                    {
                        exist = true;
                        break;
                    }
                }

                // if i exist go to new loop
                if (exist)
                {
                    continue;
                }

                // else return i
                return i;
            }

            throw new InvalidOperationException();
        }
    }
}