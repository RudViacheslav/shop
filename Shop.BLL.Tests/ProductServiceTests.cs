﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shop.BLL.Infrastructure;
using Shop.BLL.Interfaces;
using Shop.BLL.Tests.Mock;
using Shop.DAL.Entity;

namespace Shop.BLL.Tests
{
    [TestClass]
    public class ProductServiceTests
    {
        private IProductService Initialize()
        {
            var dOrders = new List<Order>
            {
                new Order {CustomerID = 0, IsFinished = true, Id = 0},
                new Order {CustomerID = 0, IsFinished = true, Id = 1},
                new Order {CustomerID = 1, IsFinished = true, Id = 2},
                new Order {CustomerID = 1, IsFinished = false, Id = 3}
            };

            var dCustomers = new List<Customer>
            {
                new Customer {Id = 0, Name = "user1"},
                new Customer {Id = 1, Name = "user2"}
            };

            var dProducts = new List<Product>
            {
                new Product {Id = 0, Cost = 100, InStock = 2, Name = "aa"},
                new Product {Id = 1, Cost = 200, InStock = 1, Name = "bb"}
            };

            var dOrderItems = new List<OrderItem>
            {
                new OrderItem {OrderId = 0, ProductId = 0, Count = 1},
                new OrderItem {OrderId = 3, ProductId = 1, Count = 2} // wrong count
            };

            var uowMock = new UnitOfWorkMock(dCustomers, dProducts, dOrders, dOrderItems);
            var productService = new ProductService(uowMock);

            return productService;
        }

        [TestMethod]
        public void GetCountTest()
        {
            var productSerivce = Initialize();
            int actual = productSerivce.GetCount();
            Assert.AreEqual(2, actual);
        }

        [TestMethod]
        public void CreateTest()
        {
            var productSerivce = Initialize();
            var expected = new Entity.Product {Name = "pp", Cost = 320, InStock = 2};
            var actual = productSerivce.Create("pp", 320, 2);
            bool match = expected.Name == actual.Name &&
                         expected.InStock == actual.InStock &&
                         expected.Cost == actual.Cost;

            Assert.AreEqual(true, match);
        }

        [TestMethod]
        public void CreateTest_nullName_throws()
        {
            var productSerivce = Initialize();
            Assert.ThrowsException<ArgumentException>(() => productSerivce.Create(null, 320, 2));
        }

        [TestMethod]
        public void CreateTest_negativeInStock_throws()
        {
            var productSerivce = Initialize();
            Assert.ThrowsException<ArgumentException>(() => productSerivce.Create("pp", 320, -1));
        }

        [TestMethod]
        public void CreateTest_negativeCost_throws()
        {
            var productSerivce = Initialize();
            Assert.ThrowsException<ArgumentException>(() => productSerivce.Create("pp", -320, 2));
        }

        [TestMethod]
        public void CreateTest_emptyName_throws()
        {
            var productSerivce = Initialize();
            Assert.ThrowsException<ArgumentException>(() => productSerivce.Create(string.Empty, 320, 2));
        }

        [TestMethod]
        public void CreateTest_spaceName_throws()
        {
            var productSerivce = Initialize();
            Assert.ThrowsException<ArgumentException>(() => productSerivce.Create(" ", 320, 2));
        }

        [TestMethod]
        public void UpdateTest()
        {
            var productService = Initialize();
            var product = productService.GetById(0);
            product.Cost = 200;
            productService.Update(product);

            var actual = productService.GetById(0);
            var expected = new Entity.Product {Id = 0, Name = "aa", Cost = 200, InStock = 2};

            bool match = expected.Id == actual.Id &&
                         expected.Name == actual.Name &&
                         expected.Cost == actual.Cost &&
                         expected.InStock == actual.InStock;

            Assert.AreEqual(true, match);
        }

        [TestMethod]
        public void UpdateTest_nullProduct_throws()
        {
            var productService = Initialize();

            Assert.ThrowsException<ArgumentNullException>(() => productService.Update(null));
        }

        [TestMethod]
        public void UpdateTest_wrongId_throws()
        {
            var productService = Initialize();

            Assert.ThrowsException<ArgumentException>(() =>
                                                          productService.Update(new Entity.Product
                                                          {
                                                              Id = 5,
                                                              Cost = 10,
                                                              Name = "pp",
                                                              InStock = 1
                                                          }));
        }

        [TestMethod]
        public void GetByIdTest()
        {
            var productService = Initialize();
            var actual = productService.GetById(0);
            var expected = new Entity.Product {Id = 0, Name = "aa", Cost = 100, InStock = 2};

            bool match = expected.Id == actual.Id &&
                         expected.Name == actual.Name &&
                         expected.Cost == actual.Cost &&
                         expected.InStock == actual.InStock;

            Assert.AreEqual(true, match);
        }

        [TestMethod]
        public void GetByIdTest_wrongId_trows()
        {
            var productService = Initialize();
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => productService.GetById(10));
        }

        [TestMethod]
        public void GetAllTest()
        {
            var productService = Initialize();
            var actual = productService.GetAll();

            var expected = new List<Entity.Product>
            {
                new Entity.Product {Id = 0, Cost = 100, InStock = 2, Name = "aa"},
                new Entity.Product {Id = 1, Cost = 200, InStock = 1, Name = "bb"}
            };

            bool match = Shared.ParallelCompare(expected, actual, (e, a) =>
                                                    e.Cost == a.Cost &&
                                                    e.Id == a.Id &&
                                                    e.Name == a.Name &&
                                                    e.InStock == a.InStock);

            Assert.AreEqual(true, match);
        }

        [TestMethod]
        public void DeleteTest()
        {
            var productService = Initialize();
            productService.Delete(0);

            var actual = productService.GetAll();

            var expected = new List<Entity.Product>
            {
                new Entity.Product {Id = 1, Cost = 200, InStock = 1, Name = "bb"}
            };

            bool match = Shared.ParallelCompare(expected, actual, (e, a) =>
                                                    e.Cost == a.Cost &&
                                                    e.Id == a.Id &&
                                                    e.Name == a.Name &&
                                                    e.InStock == a.InStock);

            Assert.AreEqual(true, match);
        }

        [TestMethod]
        public void GetAllOrderedTest_byNameAsc()
        {
            var productService = Initialize();
            var actual = productService.GetAllOrdered(2, 0, ProductOrderby.Name, true);

            var expected = new List<Entity.Product>
            {
                new Entity.Product {Id = 0, Cost = 100, InStock = 2, Name = "aa"},
                new Entity.Product {Id = 1, Cost = 200, InStock = 1, Name = "bb"}
            };

            bool match = Shared.ParallelCompare(expected, actual, (e, a) =>
                                                    e.Cost == a.Cost &&
                                                    e.Id == a.Id &&
                                                    e.Name == a.Name &&
                                                    e.InStock == a.InStock);

            Assert.AreEqual(true, match);
        }

        [TestMethod]
        public void GetAllOrderedTest_byNameDesc()
        {
            var productService = Initialize();
            var actual = productService.GetAllOrdered(2, 0, ProductOrderby.Name, false);

            var expected = new List<Entity.Product>
            {
                new Entity.Product {Id = 1, Cost = 200, InStock = 1, Name = "bb"},
                new Entity.Product {Id = 0, Cost = 100, InStock = 2, Name = "aa"}
            };

            bool match = Shared.ParallelCompare(expected, actual, (e, a) =>
                                                    e.Cost == a.Cost &&
                                                    e.Id == a.Id &&
                                                    e.Name == a.Name &&
                                                    e.InStock == a.InStock);

            Assert.AreEqual(true, match);
        }

        [TestMethod]
        public void GetAllOrderedTest_byCostAsc()
        {
            var productService = Initialize();
            var actual = productService.GetAllOrdered(2, 0, ProductOrderby.Cost, true);

            var expected = new List<Entity.Product>
            {
                new Entity.Product {Id = 0, Cost = 100, InStock = 2, Name = "aa"},
                new Entity.Product {Id = 1, Cost = 200, InStock = 1, Name = "bb"}
            };

            bool match = Shared.ParallelCompare(expected, actual, (e, a) =>
                                                    e.Cost == a.Cost &&
                                                    e.Id == a.Id &&
                                                    e.Name == a.Name &&
                                                    e.InStock == a.InStock);

            Assert.AreEqual(true, match);
        }

        [TestMethod]
        public void GetAllOrderedTest_byCostDesc()
        {
            var productService = Initialize();
            var actual = productService.GetAllOrdered(2, 0, ProductOrderby.Cost, false);

            var expected = new List<Entity.Product>
            {
                new Entity.Product {Id = 1, Cost = 200, InStock = 1, Name = "bb"},
                new Entity.Product {Id = 0, Cost = 100, InStock = 2, Name = "aa"}
            };

            bool match = Shared.ParallelCompare(expected, actual, (e, a) =>
                                                    e.Cost == a.Cost &&
                                                    e.Id == a.Id &&
                                                    e.Name == a.Name &&
                                                    e.InStock == a.InStock);

            Assert.AreEqual(true, match);
        }

        [TestMethod]
        public void GetAllOrderedTest_byInStockAsc()
        {
            var productService = Initialize();
            var actual = productService.GetAllOrdered(2, 0, ProductOrderby.InStock, true);

            var expected = new List<Entity.Product>
            {
                new Entity.Product {Id = 1, Cost = 200, InStock = 1, Name = "bb"},
                new Entity.Product {Id = 0, Cost = 100, InStock = 2, Name = "aa"}
            };

            bool match = Shared.ParallelCompare(expected, actual, (e, a) =>
                                                    e.Cost == a.Cost &&
                                                    e.Id == a.Id &&
                                                    e.Name == a.Name &&
                                                    e.InStock == a.InStock);

            Assert.AreEqual(true, match);
        }

        [TestMethod]
        public void GetAllOrderedTest_byInStockDesc()
        {
            var productService = Initialize();
            var actual = productService.GetAllOrdered(2, 0, ProductOrderby.InStock, false);

            var expected = new List<Entity.Product>
            {
                new Entity.Product {Id = 0, Cost = 100, InStock = 2, Name = "aa"},
                new Entity.Product {Id = 1, Cost = 200, InStock = 1, Name = "bb"}
            };

            bool match = Shared.ParallelCompare(expected, actual, (e, a) =>
                                                    e.Cost == a.Cost &&
                                                    e.Id == a.Id &&
                                                    e.Name == a.Name &&
                                                    e.InStock == a.InStock);

            Assert.AreEqual(true, match);
        }
    }
}