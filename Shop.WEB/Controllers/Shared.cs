﻿using Shop.BLL.Infrastructure;
using System.Web;
using System.Web.Mvc;

namespace Shop.Web.Controllers
{
    internal static class Shared
    {
        internal static (int skip, int count) GetProductsSkip(int pageNumber, int productsByPage, int totalCount)
        {
            int pageCount = totalCount / productsByPage;
            if (totalCount % pageCount != 0)
            {
                pageCount++;
            }

            int skip = pageCount * (pageNumber - 1);
            return (skip, pageCount);
        }

        internal static ProductOrderby ConvertOrderByToEnum(string orderBy)
        {
            ProductOrderby enumOrderBy;
            switch (orderBy)
            {
                default:
                case "Name":
                    enumOrderBy = ProductOrderby.Name;
                    break;
                case "Cost":
                    enumOrderBy = ProductOrderby.Cost;
                    break;
                case "InStock":
                    enumOrderBy = ProductOrderby.InStock;
                    break;
            }

            return enumOrderBy;
        }

        internal static bool CheckCookieForCustomerId(int customerId, HttpRequestBase Request)
        {
            var cookeie = Request.Cookies.Get("Shop cookie");
            var data = cookeie["LoggedInCusomerId"];
            if (cookeie["LoggedInCusomerId"] == customerId.ToString())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    
}