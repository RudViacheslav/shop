﻿using System.Web.Mvc;
using Shop.BLL.Infrastructure;
using Shop.BLL.Interfaces;
using Shop.Web.Models.Entity;
using Shop.Web.Models.ViewModels;
using BllE = Shop.BLL.Entity;

namespace Shop.Web.Controllers
{
    public class CartController : Controller
    {
        private readonly ICustomerService bllCustomers;
        private readonly IOrderItemService bllOrderItems;
        private readonly IOrderService bllOrders;
        private readonly IProductService bllPRoducts;

        public CartController(ICustomerService bllCustomers, IOrderItemService bllOrderItems, IOrderService bllOrders, IProductService bllPRoducts)
        {
            this.bllCustomers = bllCustomers;
            this.bllOrderItems = bllOrderItems;
            this.bllOrders = bllOrders;
            this.bllPRoducts = bllPRoducts;
        }

        public ActionResult Index(int customerId, int? orderId)
        {
            if (!Shared.CheckCookieForCustomerId(customerId, Request))
            {
                return RedirectToAction("Login", "Home");
            }
            CartModel model;
            model = CreateCartModel(customerId, orderId);

            return View(model);
        }

        public ActionResult ChangeCount(int customerId, int orderId, int productId, int newCount)
        {
            if (!Shared.CheckCookieForCustomerId(customerId, Request))
            {
                return RedirectToAction("Login", "Home");
            }

            try
            {
                var orderItem = bllOrderItems.GetByIds(orderId, productId);
                orderItem.Count = newCount;
                bllOrderItems.Update(orderItem);
                var model = CreateCartModel(customerId, orderId);

                return View("Index", model);
            }
            catch
            {
                return RedirectToAction("Index", new {customerId, orderId});
            }
        }

        public ActionResult DeleteOrdeItem(int customerId, int orderId, int productId)
        {
            if (!Shared.CheckCookieForCustomerId(customerId, Request))
            {
                RedirectToAction("Login", "Home");
            }
            var orderItem = bllOrderItems.GetByIds(orderId, productId);
            bllOrderItems.Delete(orderItem);
            var model = CreateCartModel(customerId, orderId);

            return View("Index", model);
        }

        public RedirectToRouteResult Buy(int customerId, int orderId)
        {
            if (!Shared.CheckCookieForCustomerId(customerId, Request))
            {
                return RedirectToAction("Login", "Home");
            }
            try
            {
                bllOrders.BuyOrder(orderId);
                return RedirectToAction("Index", "Shopping", new {customerId});
            }
            catch (OutOfStockException)
            {
                return RedirectToAction("Index", "Cart", new {customerId, orderId});
            }
        }

        [ActionName("History")]
        public ActionResult ShowHistory(int customerId)
        {
            if (!Shared.CheckCookieForCustomerId(customerId, Request))
            {
                return RedirectToAction("Login", "Home");
            }
            var customer = new Customer(bllCustomers.GetById(customerId));
            var orders = Order.Map(bllOrders.GetAllOrdersOfCustomer(customerId, true));
            customer.Orders = orders;

            return View("History", customer);
        }

        public RedirectToRouteResult BackToShopping(int customerId)
        {
            return RedirectToAction("Index", "Shopping", new {customerId});
        }


        private CartModel CreateCartModel(int customerId, int? orderId)
        {
            var customer = new Customer(bllCustomers.GetById(customerId));
            Order order;
            if (orderId != null && orderId > 0)
            {
                order = new Order(bllOrders.GetById((int) orderId));
            }
            else
            {
                order = new Order(bllOrders.GetUnfinishedOrder(customerId));
            }

            var orderItmes = OrderItem.Map(bllOrderItems.GetOrderItemsInOrder(order.Id));

            order.OrderItems = orderItmes;
            return new CartModel {Customer = customer, Order = order};
        }
    }
}