﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Shop.BLL.Interfaces;
using Shop.Web.Models.Entity;
using Shop.Web.Models.ViewModels;
using BllE = Shop.BLL.Entity;


namespace Shop.Web.Controllers
{
    public class ProductsEditorController : Controller
    {
        private readonly IProductService bllPRoducts;

        public ProductsEditorController(IProductService bllPRoducts)
        {
            this.bllPRoducts = bllPRoducts;
        }

        // GET: ProductsEditor/?pageNumber=1&orderBy=Name&ascending=True
        public ActionResult Index(int? pageNumber, string orderBy = "Name", bool ascending = true)
        {
            var model = CreateModel(pageNumber ?? 1, orderBy, ascending);
            return View(model);
        }

        // GET: ProductsEditor/Details/5
        public ActionResult Details(int id)
        {
            var model = new Product(bllPRoducts.GetById(id));
            return View(model);
        }

        // GET: ProductsEditor/Create
        public ActionResult Create()
        {
            return View(new Product { Name = "", Cost = 0, InStock = 0 });
        }

        // POST: ProductsEditor/Create
        [HttpPost]
        public ActionResult Create(string name, int? cost, int? inStock)
        {
            int inStockNn = inStock ?? 0;
            int costNn = cost ?? 0;
            try
            {
                bllPRoducts.Create(name, inStockNn, costNn);

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                var product = new Product { Name = name, Cost = inStockNn, InStock = costNn };
                ViewBag.ErrorMessage = e.Message;

                return View(product);
            }
        }

        // GET: ProductsEditor/Edit/5
        public ActionResult Edit(int id)
        {
            var bProduct = bllPRoducts.GetById(id);

            return View(new Product(bProduct));
        }

        // POST: ProductsEditor/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, string name, int? cost, int? inStock)
        {
            try
            {
                bllPRoducts.Update(new BllE.Product
                {
                    Id = id,
                    Name = name,
                    Cost = cost ?? 0,
                    InStock = inStock ?? 0,
                });
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                var bProduct = bllPRoducts.GetById(id);
                ViewBag.ErrorMessage = e.Message;

                return View(new Product(bProduct));
            }
        }

        // GET: ProductsEditor/Delete/5
        public ActionResult Delete(int id)
        {
            var model = new Product(bllPRoducts.GetById(id));
            return View(model);
        }

        // POST: ProductsEditor/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                bllPRoducts.Delete(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        private EditorModel CreateModel(int pageNumber, string orderBy = "Name", bool ascending = true)
        {
            int totalProductCount = bllPRoducts.GetCount();
            (int skip, int pageCount) = Shared.GetProductsSkip(pageNumber, 15, totalProductCount);
            var order = Shared.ConvertOrderByToEnum(orderBy);
            var products = Product.Map(bllPRoducts.GetAllOrdered(15, skip, order, ascending));

            return new EditorModel
            {
                Products = products,
                SelectedPage = pageNumber,
                PageCount = pageCount,
                SortField = orderBy,
                Ascending = ascending
            };
        }
    }
}