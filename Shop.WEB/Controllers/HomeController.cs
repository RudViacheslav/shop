﻿using System;
using System.Web;
using System.Web.Mvc;
using Shop.BLL.Interfaces;
using Shop.Web.Models.Entity;

namespace Shop.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICustomerService bllCustomers;

        public HomeController(ICustomerService bllCustomers)
        {
            this.bllCustomers = bllCustomers;
        }

        public RedirectResult Index()
        {
            return RedirectPermanent("/Home/Login");
        }

        [HttpGet]
        public ActionResult SignUp()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignUp(string name, string password)
        {
            try
            {
                bllCustomers.Create(name, password);
                var customer = new Customer(bllCustomers.TryLogin(name, password));
                HttpCookie cookie = new HttpCookie("Shop cookie");
                cookie["LoggedInCusomerId"] = customer.Id.ToString();
                Response.Cookies.Add(cookie);
                return RedirectToAction("Index", "Shopping", new { CustomerId = customer.Id });
            }
            catch (ArgumentException e)
            {
                ViewBag.ErrorMessage = e.Message;
                return View();
            }
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string userName, string password)
        {
            try
            {
                var customer = new Customer(bllCustomers.TryLogin(userName, password));
                HttpCookie cookie = new HttpCookie("Shop cookie");
                cookie["LoggedInCusomerId"] = customer.Id.ToString();
                Response.Cookies.Add(cookie);
                return Redirect($"/Shopping/Index/?CustomerId={customer.Id}");
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "Wrong username or password";
                return View();
            }
        }
    }
}