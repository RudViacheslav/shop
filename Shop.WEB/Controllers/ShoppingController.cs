﻿using System.Web;
using System.Web.Mvc;
using Shop.BLL.Infrastructure;
using Shop.BLL.Interfaces;
using Shop.Web.Models.Entity;
using Shop.Web.Models.ViewModels;

namespace Shop.Web.Controllers
{
    public class ShoppingController : Controller
    {
        private const int ProductsByPage = 10;

        private readonly ICustomerService bllCustomers;
        private readonly IOrderItemService bllOrderItems;
        private readonly IOrderService bllOrders;
        private readonly IProductService bllPRoducts;

        public ShoppingController
            (ICustomerService bllCustomers, IProductService bllPRoducts, IOrderService bllOrders, IOrderItemService bllOrderItems)
        {
            this.bllCustomers = bllCustomers;
            this.bllPRoducts = bllPRoducts;
            this.bllOrders = bllOrders;
            this.bllOrderItems = bllOrderItems;
        }

        public ActionResult Index(int customerId, int? pageNumber)
        {
            if (!Shared.CheckCookieForCustomerId(customerId, Request))
               return RedirectToAction("Login", "Home");
            var model = CreateShoppingModel(customerId, pageNumber: pageNumber ?? 1);
            return View(model);
        }

        public ActionResult IndexSort(int customerId, string orderBy, bool ascending, int? pageNumber)
        {
            if (!Shared.CheckCookieForCustomerId(customerId, Request))
                return RedirectToAction("Login", "Home");
            ShoppingModel model;
            if (pageNumber != null)
            {
                model = CreateShoppingModel(customerId, orderBy, ascending, (int)pageNumber);
            }
            else
            {
                model = CreateShoppingModel(customerId, orderBy, ascending);
            }

            return View("Index", model);
        }

        public ActionResult AddToCart(int customerId, int orderId, int productId)
        {
            if (!Shared.CheckCookieForCustomerId(customerId, Request))
                return RedirectToAction("Login", "Home");
            try
            {
                bllOrderItems.GetOrCreateOrderitem(productId, orderId);
            }
            catch (OutOfStockException)
            {
            }

            return View("Index", CreateShoppingModel(customerId));
        }

        public ActionResult LogOut()
        {
            var cookie = Request.Cookies.Get("Shop cookie");
            cookie["LoggedInCusomerId"] = "0";
            Response.SetCookie(cookie);

            return RedirectToAction("Login", "Home");
        }

        private ShoppingModel CreateShoppingModel
        (
            int customerId,
            string orderBy = "Name",
            bool ascending = true,
            int pageNumber = 1
        )
        {
            var enumOrderBy = Shared.ConvertOrderByToEnum(orderBy);

            int totalProductCount = bllPRoducts.GetCount();
            (int skip, int pageCount) = Shared.GetProductsSkip(pageNumber, ProductsByPage, totalProductCount);

            var products = Product.Map(bllPRoducts.GetAllOrdered(ProductsByPage, skip, enumOrderBy, ascending));

            var customer = new Customer(bllCustomers.GetById(customerId));

            // current corder
            var order = new Order(bllOrders.GetUnfinishedOrder(customerId));

            // Items count in cart
            int oiCount = bllOrderItems.GetOrderItemsCount(order.Id);

            return new ShoppingModel
            {
                Customer = customer,
                Order = order,
                Products = products,
                OrderItemsCount = oiCount,
                PageCount = pageCount,
                SortField = orderBy,
                Ascending = ascending,
                SelectedPage = pageNumber
            };
        }
    }
}