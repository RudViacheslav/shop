﻿using System.Web.Mvc;

namespace Shop.Web.Helpers
{
    public static class ShopHelpers
    {
        public static MvcHtmlString ShowError(this HtmlHelper htmlHelper, string errorMessage)
        {
            if (!string.IsNullOrEmpty(errorMessage))
            {
                var div = new TagBuilder("div");
                div.AddCssClass("error");
                div.Attributes.Add("style", "padding: 5px;background-color: wheat;");
                div.InnerHtml = errorMessage;
                return new MvcHtmlString(div.ToString());
            }

            return new MvcHtmlString("");
        }
    }
}