﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Shop.Web.Models.Entity;
namespace Shop.Web.Models.ViewModels
{
    public class EditorModel
    {
        public IEnumerable<Product> Products { get; set; }
        public int PageCount { get; set; }
        public int SelectedPage { get; set; }
        public string SortField { get; set; }
        public bool Ascending { get; set; }
    }
}