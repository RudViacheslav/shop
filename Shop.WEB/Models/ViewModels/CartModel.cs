﻿using Shop.Web.Models.Entity;

namespace Shop.Web.Models.ViewModels
{
    public class CartModel
    {
        public Order Order { get; set; }
        public Customer Customer { get; set; }
    }
}