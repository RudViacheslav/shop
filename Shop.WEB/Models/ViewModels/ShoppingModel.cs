﻿using System.Collections.Generic;
using Shop.Web.Models.Entity;

namespace Shop.Web.Models.ViewModels
{
    public class ShoppingModel
    {
        public Customer Customer { get; set; }
        public Order Order { get; set; }
        public int OrderItemsCount { get; set; }
        public IEnumerable<Product> Products { get; set; }

        public int PageCount { get; set; }
        public int SelectedPage { get; set; }
        public string SortField { get; set; }
        public bool Ascending { get; set; }
    }
}