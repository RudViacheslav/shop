﻿using System.Collections.Generic;
using BllE = Shop.BLL.Entity;

namespace Shop.Web.Models.Entity
{
    public class Customer
    {
        public Customer()
        {
        }

        public Customer(BllE.Customer customer)
        {
            Id = customer.Id;
            Name = customer.Name;

            Orders = Order.Map(customer.Orders);
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public IEnumerable<Order> Orders { get; set; }

        public static IEnumerable<Customer> Map(IEnumerable<BllE.Customer> customers)
        {
            var list = new List<Customer>();
            if (customers != null)
            {
                foreach (var item in customers)
                {
                    list.Add(new Customer(item));
                }
            }

            return list;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}