﻿using System.Collections.Generic;
using BllE = Shop.BLL.Entity;

namespace Shop.Web.Models.Entity
{
    public class OrderItem
    {
        public OrderItem()
        {
        }

        public OrderItem(BllE.OrderItem orderItem)
        {
            Count = orderItem.Count;
            ProductId = orderItem.ProductId;
            OrderId = orderItem.OrderId;

            Product = new Product(orderItem.Product);
            Order = new Order(orderItem.Order);
        }

        public int Count { get; set; }
        public int ProductId { get; set; }
        public int OrderId { get; set; }

        public Product Product { get; set; }
        public Order Order { get; set; }

        public int GetAmount()
        {
            return Product.Cost * Count;
        }

        public static IEnumerable<OrderItem> Map(IEnumerable<BllE.OrderItem> orderItems)
        {
            var list = new List<OrderItem>();
            if (orderItems != null)
            {
                foreach (var item in orderItems)
                {
                    list.Add(new OrderItem(item));
                }
            }

            return list;
        }
    }
}