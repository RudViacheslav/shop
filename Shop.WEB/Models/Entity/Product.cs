﻿using System.Collections.Generic;
using BllE = Shop.BLL.Entity;

namespace Shop.Web.Models.Entity
{
    public class Product
    {
        public Product()
        {
        }

        public Product(BllE.Product product)
        {
            Id = product.Id;
            Name = product.Name;
            Cost = product.Cost;
            InStock = product.InStock;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int Cost { get; set; }

        /// <summary>
        ///     Count of products in stock.
        /// </summary>
        public int InStock { get; set; }

        public static IEnumerable<Product> Map(IEnumerable<BllE.Product> products)
        {
            var list = new List<Product>();
            if (products != null)
            {
                foreach (var item in products)
                {
                    list.Add(new Product(item));
                }
            }

            return list;
        }
    }
}