﻿using System.Collections.Generic;
using BllE = Shop.BLL.Entity;

namespace Shop.Web.Models.Entity
{
    public class Order
    {
        public Order()
        {
        }

        public Order(BllE.Order order)
        {
            Id = order.Id;
            CustomerID = order.CustomerID;
            IsFinished = order.IsFinished;

            OrderItems = OrderItem.Map(order.OrderItems);
        }


        public int Id { get; set; }
        public int CustomerID { get; set; }
        public bool IsFinished { get; set; }

        public Customer Customer { get; set; }
        public IEnumerable<OrderItem> OrderItems { get; set; }

        public int GetAmount()
        {
            int sum = 0;

            foreach (var item in OrderItems)
            {
                sum += item.GetAmount();
            }

            return sum;
        }

        public static IEnumerable<Order> Map(IEnumerable<BllE.Order> orders)
        {
            var list = new List<Order>();
            if (orders != null)
            {
                foreach (var item in orders)
                {
                    list.Add(new Order(item));
                }
            }

            return list;
        }
    }
}