﻿using System;
using Shop.DAL.Entity;

namespace Shop.DAL.Interfaces
{
    /// <summary>
    ///     Provides access to repositories with same context
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        Repository<Product> Products { get; }
        Repository<Customer> Customers { get; }
        Repository<Order> Orders { get; }
        Repository<OrderItem> OrderItems { get; }

        /// <summary>
        ///     Fixes all changes in DB.
        /// </summary>
        void SaveChanges();
    }
}