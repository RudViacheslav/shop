﻿using System.Linq;
using Shop.DAL.EF;

namespace Shop.DAL.Interfaces
{
    /// <summary>
    ///     Represents the repository of T. Provides methods to manipulate entities of T int DB.
    /// </summary>
    /// <typeparam name="T">Type of Entity.</typeparam>
    public abstract class Repository<T>
    {
        protected ModelContext db;

        public Repository(ModelContext modelContext)
        {
            db = modelContext;
        }

        public abstract IQueryable<T> All { get; }

        public abstract T Create(T item);
        public abstract void Delete(T item);
        public abstract void Delete(params object[] key);
        public abstract T Read(params object[] key);
        public abstract void Update(T item);
    }
}