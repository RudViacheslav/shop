﻿using System.ComponentModel.DataAnnotations;

namespace Shop.DAL.Entity
{
    public class Product
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Range(0, int.MaxValue)]
        [Required]
        public int Cost { get; set; }

        /// <summary>
        ///     Count of products in stock.
        /// </summary>
        [Range(0, int.MaxValue)]
        public int InStock { get; set; }
    }
}