﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Shop.DAL.Entity
{
    public class Customer
    {
        public Customer()
        {
            Orders = new HashSet<Order>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        [MinLength(4)]
        public string Password { get; set; }

        public ICollection<Order> Orders { get; set; }
    }
}