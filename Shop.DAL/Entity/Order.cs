﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Shop.DAL.Entity
{
    public class Order
    {
        public Order()
        {
            OrderItems = new HashSet<OrderItem>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public int CustomerID { get; set; }

        public bool IsFinished { get; set; }

        public Customer Customer { get; set; }

        public ICollection<OrderItem> OrderItems { get; set; }
    }
}