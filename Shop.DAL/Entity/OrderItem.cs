﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Shop.DAL.Entity
{
    public class OrderItem
    {
        public int Count { get; set; }

        [Key]
        [Column(Order = 2)]
        public int ProductId { get; set; }

        [Key]
        [Column(Order = 1)]
        public int OrderId { get; set; }

        public Product Product { get; set; }
        public Order Order { get; set; }
    }
}