﻿using System;
using Shop.DAL.EF;
using Shop.DAL.Entity;
using Shop.DAL.Interfaces;
using Shop.DAL.Repositories;

namespace Shop.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly CustomerRepository customers;
        private readonly ModelContext db;
        private readonly OrderItemRepository orderItems;
        private readonly OrderRepository orders;
        private readonly ProductsRepository products;

        private bool disposed;

        public UnitOfWork()
        {
            db = new ModelContext("DefaultConnection");
            customers = new CustomerRepository(db);
            orders = new OrderRepository(db);
            orderItems = new OrderItemRepository(db);
            products = new ProductsRepository(db);
        }

        public UnitOfWork(string connectionString)
        {
            db = new ModelContext(connectionString);
            customers = new CustomerRepository(db);
            orders = new OrderRepository(db);
            orderItems = new OrderItemRepository(db);
            products = new ProductsRepository(db);
        }

        public Repository<Customer> Customers => customers;

        public Repository<Order> Orders => orders;

        public Repository<OrderItem> OrderItems => orderItems;


        public Repository<Product> Products => products;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void SaveChanges()
        {
            db.SaveChanges();
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }

                disposed = true;
            }
        }
    }
}