﻿using System.Data.Entity;
using Shop.DAL.Entity;

namespace Shop.DAL.EF
{
    public class ModelInitializer : DropCreateDatabaseAlways<ModelContext>
    {
        protected override void Seed(ModelContext context)
        {
            context.Customers.Add(new Customer {Name = "User1", Password="1111"});

            context.Products.Add(new Product {Name = "Samsung RB37J5000SA", Cost = 14_049, InStock = 20});
            context.Products.Add(new Product {Name = "Sharp SJ-X640-HS3", Cost = 16_699, InStock = 0});
            context.Products.Add(new Product {Name = "Bosch KGN39XL35", Cost = 12_861, InStock = 5});
            context.Products.Add(new Product {Name = "Beko RCNA355E21W", Cost = 10_881, InStock = 3});
            context.Products.Add(new Product {Name = "Samsung RB30J3000WW", Cost = 3_801, InStock = 10});
            context.Products.Add(new Product {Name = "Delfa DF-85", Cost = 11_820, InStock = 14});
            context.Products.Add(new Product {Name = "Samsung RB30J3000SA", Cost = 7_138, InStock = 2});
            context.Products.Add(new Product {Name = "Sharp SJ-B1239M4W", Cost = 4_572, InStock = 8});
            context.Products.Add(new Product {Name = "Whirlpool ART 9811/A++ SF", Cost = 17_431, InStock = 6});
            context.Products.Add(new Product {Name = "Whirlpool SP40 801 EU", Cost = 25_398, InStock = 4});
            context.Products.Add(new Product {Name = "Samsung RB29FSRNDSA", Cost = 12_706, InStock = 28});
            context.Products.Add(new Product {Name = "ATLANT МХМ 2835-9", Cost = 6_861, InStock = 12});
            context.Products.Add(new Product {Name = "Bosch KGN39ML3", Cost = 14_206, InStock = 0});
            context.Products.Add(new Product {Name = "Elenberg MRF-221-O", Cost = 4_881, InStock = 5});

            context.Products.Add(new Product {Name = "Beko RFSE200T20W", Cost = 7_240, InStock = 5});
            context.Products.Add(new Product {Name = "Samsung RB30J3000SA", Cost = 11_392, InStock = 8});
            context.Products.Add(new Product {Name = "Samsung RS57K4000SA", Cost = 22_402, InStock = 4});
            context.Products.Add(new Product {Name = "ATLANT МХМ 2835-95", Cost = 6_930, InStock = 7});
            context.Products.Add(new Product {Name = "ERGO MR-130", Cost = 4_598, InStock = 15});
            context.Products.Add(new Product {Name = "ATLANT ХМ 4625-101", Cost = 10_202, InStock = 3});
            context.Products.Add(new Product {Name = "Delfa DMF-83", Cost = 3_459, InStock = 4});
            context.Products.Add(new Product {Name = "Liebherr CU 3311", Cost = 10_176, InStock = 9});
            context.Products.Add(new Product {Name = "Indesit LR8 S1 X", Cost = 9_186, InStock = 8});
            context.Products.Add(new Product {Name = "Snaige RF35SM-S10021", Cost = 8_773, InStock = 10});
            context.Products.Add(new Product {Name = "Samsung BRB260010WW", Cost = 13_672, InStock = 4});
            context.Products.Add(new Product {Name = "LG GA-B429SEQZ", Cost = 15_122, InStock = 8});
            context.Products.Add(new Product {Name = "LG GA-B499YYUZ", Cost = 16_689, InStock = 2});
            context.Products.Add(new Product {Name = "Indesit NTS 14 AA", Cost = 6_547, InStock = 1});
            context.Products.Add(new Product {Name = "Whirlpool BLF 8121 W", Cost = 9_246, InStock = 16});
            context.Products.Add(new Product {Name = "Delfa BCD-138", Cost = 4_666, InStock = 4});
            context.Products.Add(new Product {Name = "Whirlpool BSNF 9121 OX", Cost = 12_514, InStock = 6});
            context.Products.Add(new Product {Name = "Samsung RZ32M7125S9", Cost = 17_547, InStock = 5});
            context.Products.Add(new Product {Name = "Indesit DF 4181 W", Cost = 10_648, InStock = 2});
            context.Products.Add(new Product {Name = "Indesit LR6 S1 X", Cost = 7_725, InStock = 5});
            context.Products.Add(new Product {Name = "Samsung RB41J7839S4", Cost = 24_885, InStock = 5});
            context.Products.Add(new Product {Name = "Samsung RB29FSRNDWW", Cost = 11_760, InStock = 6});
            context.Products.Add(new Product {Name = "Hotpoint-Ariston E4D AA X C", Cost = 19_106, InStock = 0});
            context.Products.Add(new Product {Name = "Liebherr CN 4015", Cost = 18_237, InStock = 3});

            context.Products.Add(new Product {Name = "LG GA-B429SECZ", Cost = 14_325, InStock = 1});
            context.Products.Add(new Product {Name = "Zanussi ZFU19400WA", Cost = 7_589, InStock = 0});
            context.Products.Add(new Product {Name = "Bosch KGN36VL306", Cost = 14_733, InStock = 10});
            context.Products.Add(new Product {Name = "Bosch KGN39VW306", Cost = 16_374, InStock = 1});
            context.Products.Add(new Product {Name = "Indesit NUS 16.1 AA NF H", Cost = 9_494, InStock = 8});
            context.Products.Add(new Product {Name = "Snaige F27SM-T10001", Cost = 7_888, InStock = 5});
            context.Products.Add(new Product {Name = "Indesit TFAA 5", Cost = 4_452, InStock = 2});
            context.Products.Add(new Product {Name = "Samsung RB37J5010SA", Cost = 12_710, InStock = 3});
            context.Products.Add(new Product {Name = "Elenberg MRF-146-O", Cost = 4_635, InStock = 1});
            context.Products.Add(new Product {Name = "LG GA-B429SMQZ", Cost = 15_322, InStock = 9});
            context.Products.Add(new Product {Name = "Beko RFNE 312E23 W", Cost = 13_213, InStock = 4});
            context.Products.Add(new Product {Name = "ATLANT ХМ 6026-100", Cost = 10_147, InStock = 5});
            context.Products.Add(new Product {Name = "Whirlpool BSNF 9152 W", Cost = 12_471, InStock = 4});
            context.Products.Add(new Product {Name = "Samsung RB31FERNDEF", Cost = 12_628, InStock = 8});
            context.Products.Add(new Product {Name = "Nord T 271 (W)", Cost = 5_389, InStock = 5});
            context.Products.Add(new Product {Name = "Bosch KGN49XI30U", Cost = 23_968, InStock = 7});
            context.Products.Add(new Product {Name = "Zanussi ZFC26400WA", Cost = 7_271, InStock = 1});
            context.Products.Add(new Product {Name = "Zanussi ZFG06400WA", Cost = 5_074, InStock = 7});
            context.Products.Add(new Product {Name = "Samsung RB33J3230BC", Cost = 12_699, InStock = 5});
            context.Products.Add(new Product {Name = "Indesit IBS 18 AA (UA)", Cost = 7_835, InStock = 2});
            context.Products.Add(new Product {Name = "Zanussi ZRA40100WA", Cost = 9_546, InStock = 1});
            context.Products.Add(new Product {Name = "Whirlpool ART 963/A+/NF", Cost = 19_903, InStock = 5});
            context.Products.Add(new Product {Name = "LG GA-B389SMQZ", Cost = 13_517, InStock = 0});
            context.Products.Add(new Product {Name = "Mystery MRF-8070W", Cost = 3_128, InStock = 2});

            context.SaveChanges();
        }
    }
}