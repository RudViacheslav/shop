﻿using System.Data.Entity;
using Shop.DAL.Entity;

namespace Shop.DAL.EF
{
    public class ModelContext : DbContext
    {
        public ModelContext() : base("DefaultConnection")
        {
        }

        public ModelContext(string connectionString) : base(connectionString)
        {
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
#if DEBUG
        private static int s_instanceCounter;
        private readonly int _instanceIndex = s_instanceCounter++;
#endif
    }
}