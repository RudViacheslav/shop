﻿using System.Data.Entity;
using System.Linq;
using Shop.DAL.EF;
using Shop.DAL.Entity;
using Shop.DAL.Interfaces;

namespace Shop.DAL.Repositories
{
    public class OrderItemRepository : Repository<OrderItem>
    {
        public OrderItemRepository(ModelContext modelContext) : base(modelContext)
        {
        }

        public override IQueryable<OrderItem> All => db.OrderItems;

        public override OrderItem Create(OrderItem item)
        {
            return db.OrderItems.Add(item);
        }

        public override OrderItem Read(params object[] keyValues)
        {
            return db.OrderItems.Find(keyValues);
        }

        public override void Update(OrderItem item)
        {
            db.Entry(item).State = EntityState.Modified;
        }

        public override void Delete(OrderItem item)
        {
            db.OrderItems.Remove(item);
        }

        public override void Delete(params object[] id)
        {
            var item = Read(id);
            if (item != null)
            {
                db.OrderItems.Remove(item);
            }
        }
    }
}