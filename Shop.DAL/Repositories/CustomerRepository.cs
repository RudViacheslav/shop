﻿using System.Data.Entity;
using System.Linq;
using Shop.DAL.EF;
using Shop.DAL.Entity;
using Shop.DAL.Interfaces;

namespace Shop.DAL.Repositories
{
    public class CustomerRepository : Repository<Customer>
    {
        public CustomerRepository(ModelContext modelContext) : base(modelContext)
        {
        }

        public override IQueryable<Customer> All => db.Customers;

        public override Customer Create(Customer item)
        {
            return db.Customers.Add(item);
        }

        public override Customer Read(params object[] id)
        {
            return db.Customers.Find(id);
        }

        public override void Update(Customer item)
        {
            db.Entry(item).State = EntityState.Modified;
        }

        public override void Delete(Customer item)
        {
            db.Customers.Remove(item);
        }

        public override void Delete(params object[] id)
        {
            var item = Read(id);
            if (item != null)
            {
                db.Customers.Remove(item);
            }
        }
    }
}