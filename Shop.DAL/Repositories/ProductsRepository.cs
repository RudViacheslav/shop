﻿using System.Data.Entity;
using System.Linq;
using Shop.DAL.EF;
using Shop.DAL.Entity;
using Shop.DAL.Interfaces;

namespace Shop.DAL.Repositories
{
    internal class ProductsRepository : Repository<Product>
    {
        public ProductsRepository(ModelContext modelContext) : base(modelContext)
        {
        }

        public override IQueryable<Product> All => db.Products;

        public override Product Create(Product item)
        {
            return db.Products.Add(item);
        }

        public override Product Read(params object[] id)
        {
            return db.Products.Find(id);
        }

        public override void Update(Product item)
        {
            db.Entry(item).State = EntityState.Modified;
        }

        public override void Delete(Product item)
        {
            db.Products.Remove(item);
        }

        public override void Delete(params object[] id)
        {
            var item = Read(id);
            if (item != null)
            {
                db.Products.Remove(item);
            }
        }
    }
}