﻿using System.Data.Entity;
using System.Linq;
using Shop.DAL.EF;
using Shop.DAL.Entity;
using Shop.DAL.Interfaces;

namespace Shop.DAL.Repositories
{
    public class OrderRepository : Repository<Order>
    {
        public OrderRepository(ModelContext modelContext) : base(modelContext)
        {
        }

        public override IQueryable<Order> All => db.Orders;

        public override Order Create(Order item)
        {
            return db.Orders.Add(item);
        }

        public override Order Read(params object[] id)
        {
            return db.Orders.Find(id);
        }

        public override void Update(Order item)
        {
            db.Entry(item).State = EntityState.Modified;
        }

        public override void Delete(Order item)
        {
            db.Orders.Remove(item);
        }

        public override void Delete(params object[] id)
        {
            var item = Read(id);
            if (item != null)
            {
                db.Orders.Remove(item);
            }
        }
    }
}